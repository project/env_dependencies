<?php

namespace Drupal\env_dependencies\Commands;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\Dependency;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ProfileExtensionList;
use Drupal\env_dependencies\EnvDependenciesEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This is a literal copy of the example Symfony Console command
 * from the documentation.
 *
 * See:
 * http://symfony.com/doc/2.7/components/console/introduction.html#creating-a-basic-command
 */
class EnvDependenciesCommand extends Command {

  protected $cache;

  protected $eventDispatcher;

  protected $moduleInstaller;

  protected $extensionListModule;

  protected $extensionListProfile;

  /**
   * EnvDependenciesCommand constructor.
   *
   * @param string|null $name
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $moduleInstaller
   * @param \Drupal\Core\Extension\ModuleExtensionList $extensionListModule
   * @param \Drupal\Core\Extension\ProfileExtensionList $extensionListProfile
   */
  public function __construct(string $name = NULL, CacheBackendInterface $cache, ConfigFactoryInterface $config, ContainerAwareEventDispatcher $eventDispatcher, ModuleInstallerInterface $moduleInstaller, ModuleExtensionList $extensionListModule, ProfileExtensionList $extensionListProfile) {
    parent::__construct($name);
    $this->cache = $cache;
    $this->eventDispatcher = $eventDispatcher;
    $this->moduleInstaller = $moduleInstaller;
    $this->extensionListModule = $extensionListModule;
    $this->extensionListProfile = $extensionListProfile;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('env-dependencies:run')
      ->setDescription('Run environment')
      ->addOption(
        'status_only',
        NULL,
        InputOption::VALUE_NONE,
        'If set, the task will only show the changing status'
      )
      ->addOption(
        'environment',
        NULL,
        InputOption::VALUE_OPTIONAL,
        'If set it will load the environment name variable from cli instead of config'
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    // Remove info cache.
    $this->extensionListProfile->reset();
    $this->extensionListModule->reset();

    $config = \Drupal::config('env_dependencies.settings');
    $environment = $input->getOption('environment');

    if (empty($environment)) {
      $environment = $config->get('environment');
    }
    if (!$input->getOption('status_only')) {
      $output->writeln('Switching to ' . $environment);
    }
    else {
      $output->writeln('Switching to status: ' . $environment);
    }

    // Environment.
    $event = new EnvDependenciesEvent($environment, $config);
    $modules = $this->extensionListModule->getAllInstalledInfo();
    $default_dependencies = [];
    $enable_modules = [];

    if ($profile = \Drupal::installProfile()) {
      $default_dependencies[$profile] = TRUE;
      /** @var \Drupal\Core\Extension\ProfileExtensionList $profile_extension_list */
      $profile_extension_list = \Drupal::service('extension.list.profile');
      $list_profiles = $profile_extension_list->getAllInstalledInfo();
      foreach ($list_profiles as $profile => $profile_info) {
        if (method_exists($profile_extension_list, 'getAncestors')) {
          foreach ($profile_extension_list->getAncestors($profile) as $key => $profile) {
            $default_dependencies[$key] = TRUE;
          }
        }
      }
    }

    foreach ($modules as $module_name => $module) {
      if (!empty($module['dependencies']) || !empty($module['install'])) {
        foreach (array_merge($module['dependencies'] ?? [], $module['install'] ?? []) as $dependency) {
          $dependency = Dependency::createFromString($dependency);

          if (array_key_exists($dependency->getName(), $modules)) {
            $default_dependencies[$dependency->getName()] = TRUE;
          }
          else {
            $default_dependencies[$dependency->getName()] = FALSE;
            $enable_modules[$dependency->getName()] = TRUE;
          }
        }
      }

      if (!empty($module[$environment . '_dependencies'])) {
        foreach ($module[$environment . '_dependencies'] as $dependency) {
          $dependency = Dependency::createFromString($dependency);
          if (array_key_exists($dependency->getName(), $modules)) {
            $default_dependencies[$dependency->getName()] = TRUE;
          }
          else {
            $default_dependencies[$dependency->getName()] = FALSE;
            $enable_modules[$dependency->getName()] = TRUE;
          }
        };
      }
    }

    $moduleInstaller = \Drupal::service('module_installer');
    if (count($enable_modules)) {
      $output->writeln('Enabling modules: ' . implode(',', array_keys($enable_modules)));
      $this->eventDispatcher->dispatch(EnvDependenciesEvent::BEFORE_ENABLE_DEPENDENCIES, $event);
      if (!$input->getOption('status_only')) {
        $moduleInstaller->install(array_keys($enable_modules));
      }
    }

    $uninstall_modules = array_diff(array_keys($modules), array_keys($default_dependencies), array_keys($enable_modules));
    if (count($uninstall_modules)) {
      $output->writeln('Uninstall modules: ' . implode(',', $uninstall_modules));
      if (!$input->getOption('status_only')) {
        $moduleInstaller->uninstall($uninstall_modules, FALSE);
      }
    }

    if (!$input->getOption('status_only')) {
      $this->eventDispatcher->dispatch(EnvDependenciesEvent::AFTER_ENABLE_DEPENDENCIES, $event);
      $output->writeln('Switched to: ' . $environment);
    }
  }

}
